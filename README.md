### Documentation template

This MkDocs template was forked from the Our Sci template project. You can use it to create a static HTML webpage of whatever documentation you save in markdown files in this repository.

### HTML webpage information

   - site_name: PCSC Event Storming Documentation
   - site_url: https://openteamag.gitlab.io/codesign/pcsc/

Now you can add markdown files to your repository and the index will match the folder structure of your files!

### Learn more about mkdocs generally 

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.
