# Participating Orgs

## USDA PCSC Projects: 

- [Growing GRASS](https://www.ggrass.org/), represented by Growing Grass, IC-Foods and University of California - Davis
- [Action for Climate-Smart Agriculture](https://www.wolfesneck.org/partnerships-for-climate-smart-commodities/), represented by Wolfe’s Neck Center for Agriculture & the Environment
- [Climate Beneficial Fiber Partnership](https://fiberpartnership.ncat.org/), represented by Fibershed, National Center for Appropriate Technology, and Carbon Cycle Institute
- [Climate-Smart Beef](https://farmland.org/project/climate-smart-beef/), represented by American Farmland Trust
- [Climate-Smart Farming & Marketing](https://climatesmart.org/), represented by Pasa
- [Building Capacity for Conservation Districts](https://www.nacdnet.org/about-nacd/what-we-do/climate-smart-commodities-project/), represented by National Association of Conservation Districts
- [Transforming the Farmer to Consumer Supply Chain](https://www.transformf2c.com/), represented by Carbon A-List

### Developer teams

We also invited independent developers who were not affiliated with a specific PCSC project, but were working on solutions related to these issues. Developer affiliations included: 

- [farmOS](https://farmos.org/)
- [Our-Sci, LLC](https://www.our-sci.net/)
- [Purdue Agricultural Informatics lab](http://aginformaticslab.org/)
- [Semios](https://semios.com/)
- [Terran Collective](https://www.terran.io/)
- [Qlever](https://www.qlever.com/)
- [Regen Network](https://www.regen.network/)

## Project Details

### Growing Grass
- [Project Site](https://www.ggrass.org/)
- [Roots of Change - Growing GRASS](https://www.rootsofchange.org/projects/growing-grass-usda-climate-smart-commodity-grant-project/) 

#### USDA Description

This multi-year project will pilot, test, and evaluate how the GRASS supply chain can be optimized for value and climate performance from farm and ranch to climate-smart markets, starting with the greenhouse gas benefits of grazing systems.

**Lead Partner**: American Sustainable Business Institute Inc.

**Other Major Partners**: Roots of Change, CA Catlemen's Association, American Grassfed Association, Pure Strategies, UC Davis Food Systems Lab, IC-Foods, Autocase, Health Research Institute Labs, Other Half Processing SBC, Regenerative Rising, Lookin.to, Textile Exchange

<br>

![Growing Grass](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/growing_grass2.jpg?ref_type=heads)

<br>

**Work description for Event Storming**


* Our mission is to increase the supply value and utilization of regeneratively raised beef cattle and buffalo hides and other materials that are byproducts from meat processing.

* The goal of our project is to build out this byproduct supply chain with feedback loops in a way that value carries through to processors and producers in a way that's sustainable, so that market partners are happy, and ideally there's policy support for this space as well.

*  The goal is twofold. One is to be able to ask and answer these upstream and downstream questions about how food systems are interrelated with conservation work, and also to produce a cyber infrastructure that democratizes food systems by enabling a greater number and diverse diversity of food systems actors.



### Action for Climate-Smart Agriculture
- [Project Site](https://www.wolfesneck.org/partnerships-for-climate-smart-commodities/)

#### USDA Description

This project will develop the systemic tools and approaches necessary to catalyze change by operating in three areas simultaneously: equipping and training Technical Service Providers for CSA implementation, creating transition finance incentives for producers, and developing a robust and self-sustaining marketplace for climate-smart commodities. A market expansion strategy is planned to leverage the partnership networks to expand purchaser commitments, develop transition financing models and a CSA connector and marketplace exchange to match buyers, funders and producers and implement community engagement and consumer marketing.  

**Lead Partner**: The Wolfe's Neck Farm Foundation, Inc.

**Other Major Partners:** Zero Food Print, Point Blue, Permanent.ag, LookINTO, FarmOS,  Our Sci, LLC,  Purdue University, Regen Network, Tech Matters, Terran Collective



**Work description for Event Storming:**

* The goal of our project is that we're trying to build markets for climate smart commodities to withstand and expand past this federal investment. Basically, we know we have federal dollars right now– what will be the lever to continue to incentivize regeneration five years from now? 



### Climate Beneficial Fiber Partnership
- [Project Site](https://fiberpartnership.ncat.org/)

#### USDA Description

This project will expand the existing Climate Beneficial™ fiber program: a system for sequestering carbon, regenerating soil health and resilience, improving social equity, and bolstering America's ability to produce climate-smart fiber. A newly created, open-source, Carbon Farm Planning and Verification Platform will streamline climate-smart agriculture planning and verification for producers, verifiers, and supply chain stakeholders.

**Lead Partner:** National Center for Appropriate Technology, Inc.

**Other Major Partners**: Carbon Cycle Institute, Colorado State University Dept of Soil and Crop Sciences, Fibershed, Seed 2 Shirt, New York Textile Lab

**Work description for Event Storming:**

* We're focused on wool and cotton commodities, marrying federal dollars with market development to bring premium fibers into the marketplace and create a sustainable business model for that, while always driving more value back to the growers

*  Our modeling is predominantly through COMET-planner, and we're working with CSU to develop a portal to do the carbon farm planning process for the growers, which has historically been done. by hand with modeling and COMET-planner...my goal,or my dream outcome on this is finding a way to partner to get our needs met, and also to reduce duplication of effort. 



### Climate Smart Beef
- [Project Site](https://farmland.org/project/climate-smart-beef/)

#### USDA Description

This eight-state project will amplify production of climate-smart beef by expanding market drivers, grassroots support networks, and early adopter mentors and providing technical assistance for the adoption of climate-smart grazing practices to substantially reduce agricultural greenhouse gas emissions and increase carbon sequestration. The project plans 90 percent of participants will be small-scale producers.

**Lead Partner:** American Farmland Trust

**Other Major Partners:** The Integrity Beef Alliance, Indigo Ag, AgriWebb, Earth Optics, Regenified, OpenTEAM, U.S. Biochar Initiative, Virginia Forage and Grasslands Council, Maryland Grazers Network, Pennsylvania Grazing Lands Coalition, Mountains to Bay Grazing Alliance, Black Family Land Trust, Minority & Veteran Farmers of the Piedmont, and Farmer Veteran Coalition

<br>

![AFT](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/aft2.jpg?ref_type=heads)

<br>


**Work description for Event Storming:** 

* We are partnering with US beef producers to create lasting climate smart beef markets. The goal is to get climate smart beef to market, so producers can get a premium. We are working with both large producers in places like Oklahoma and Texas, as well as small-scale producers in the Southeast and Mid-atlantic, including historically underserved producers. We intend to build that market so that there are places and corporations that want to buy.


### Climate-Smart Farming & Marketing

- [Project Site](https://climatesmart.org/)

#### USDA PCSC Description:  

This project brings together 20 farming and agroforestry organizations, serving over 20,000 small to mid- scale and underserved farmers who are uniquely impacted by climate change. The project will include soil health and financial benchmark community science; peer-to-peer learning and support; expanded implementation of climate-smart practices; carbon benefits calculation and verification; and income stream innovations that result in increased sales from farms and forest lands that use and promote climate-smart practices. 

**Lead Partner:** Pennsylvania Association for Sustainable Agriculture

**Other Major Partners:** Carolina Farm Stewardship Association, Community Involved in Sustaining Agriculture, OurSci-SurveyStack/FarmOS, Future Harvest, Maine Farmland Trust, ME Organic Farmers and Gardeners Association, Northeast Organic Farmers Association (NOFA) - CT, MA, NH, NJ, NY, RI, VT, OpenTEAM, Pennsylvania Certified Organic, Ramapough Lunaape Nation Turtle Clan, Kimberton Whole Foods, Kitchen Table Consultants, National Fish and Wildlife Foundation, National Food MarketMaker Program, Pa Flax, Pennsylvania Department of Agriculture, PA Department of Conservation and Natural Resources, PA Soil Health Coalition, Stroud Water Resource Center, TeamAg, Ramapough Lenape, Nanticoke Lenni-Lenape, Houlton Band of Maliseet, Mi'kmaq, Penobscot, Passamaquoddy, Pocasset Pokanoket, Powhatan Renape, Pocasset Wampanoag

<br>

![Pasa Map](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/pasa_map.jpg?ref_type=heads)

<br>


**Work description for Event Storming:**

* Our mission is to cultivate environmentally sound, economically viable, and community-focused farms and food systems, before and beyond PCSC.

* The goals for this project for us are to support climate conscious farmers in our network and also to measure the environmental benefits of climate smart practices and to grow consumer demand for climate smart products. 

* The best outcome would be to understand what all these other projects are and what people are doing, and especially down the line, how we can all integrate together, and what tools can be available to help with things like our benchmark studies in terms of getting firmer records.



### Building Capacity for Conservation Districts
- [Project site](https://www.nacdnet.org/about-nacd/what-we-do/climate-smart-commodities-project/)

#### USDA PCSC Description: 

This project will work through its network of 3,000 conservation districts throughout the nation to grow and advance grassroots efforts to ensure producers and local communities are prepared to meet the demand and have access to climate-smart commodity markets. Project plans to support implementation of climate-smart practices like cover crops, nutrient management plans, forest stand management, prescribed grazing and forage and biomass planting.

**Lead Partner:** National Association of Conservation Districts

**Other Major Partners:** Indian Nations Conservation Alliance (INCA), Rural Coalition, the Kansas Black Farmers Association, and the Rural Advancement Fund of the National Sharecroppers Fund, Ecosystem Services Market Consortium (ESMC), Field to Market: The Alliance for Sustainable Agriculture, HabiTerre, Cornell University Atkinson Center for Sustainability


**Work description for Event Storming:** 

*  Our project is fostering public-private partnerships, working to get conservation plans and new acres enrolled. NACD’s mission is to promote responsible resource management through the locally led efforts at the conservation district level.


### Transforming the Farmer to Consumer Supply Chain

- [Project site](https://www.transformf2c.com/)

#### USDA PCSC Description: 

This project will focus on creating end-to-end supply chain partnerships to optimize the value of climate- smart commodities, focusing on dairy feedstock and including a manure management component.

**Lead Partner:** Carbon A List LLC

**Other Major Partners:** Danone North America, over 350 independent U.S. Farmers, Target, Sustainable Environmental Consultants (SEC), University of Wisconsin, Verra, Beck’s Superior Hybrids, Inc., Scoular, Swampy Hollow Farms, McCarty Oats, Lakeview Organic Grains

<br>

![Carbon A-List](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/carbonalist.jpg?ref_type=heads)

<br>

**Work description for Event Storming:** 

* Our mission is to empower individuals and organizations with the knowledge and resources needed to drive climate action. 

* The goal of the project is to transform the farmer to consumer supply chain in building a replicable model that goes from seed to fork in tracking and differentiating the products in our project. Our focus is the dairy, oats, and soy supply chain. We have a modest goal of 50,000 metric tons of carbon dioxide removed and reduced. We have a few incentive pools for farmers who could stack functions and supporting functions across different identified NRCS conservation codes.

* The best outcomes from this event would be learning a bit more about the alternatives and solutions to some challenges that we've experienced with data collection, and learn more about opportunities collectively to address the small and underserved aspects of that data collection.

* The worst outcome for us and for everyone is that we all leave and do the exact same thing we were going to do anyway, and replicate without recognizing the answers are in front of us.




#### Sources
*All USDA project information sourced from [USDA Partnerships for Climate-Smart Commodities Project Summaries](https://www.usda.gov/climate-solutions/climate-smart-commodities/projects), accessed January 2024.* 

*Links to individual program pages were updated January 2024.*

*Work descriptions for Event Storming were based on transcripts recorded Dec. 12, 2023 in Oakland, CA. Comments have been edited for length and clarity. You can see these statements in context [here](https://docs.google.com/document/d/1lZiBuXziXakoyRFJexME3yc0f0flWpsB2Nu_veSaIOU/).*

