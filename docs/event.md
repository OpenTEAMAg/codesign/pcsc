# Event

## Kickoff 

**Developer Kickoff meeting, November 14th, 2023**

On November 14th, we held a kickoff meeting for our developer teams to provide context on PCSC and the problems we were thinking through with this event. 


**Preparation & Context for in-person work** - What are our minimal common elements?

* [Background Document](https://docs.google.com/document/d/1SUAR2R2_68_r1bgXsjDfNZsFgwfrvfAI_y-uETeBqXE/edit#heading=h.rc5et4fkdcce)
* [Narrative/Notes - Vic](https://docs.google.com/document/d/1cZ5CO_zCCw36wiZDsY7_Hs_lwbalVb1S65qE1Yf62QI/edit)
* [Event Miro](https://miro.com/app/board/uXjVNPby6DM=/)


## Event Storming 

**In-Person Event - December 12th-13th, 2023**

*Held at [Good Mother Studio](https://goodmothergallery.com/) in Oakland, CA*

<br>

![group 5](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/group5.jpg?ref_type=heads)

<br>

### Day 1 

**Event Storming**: Document data processes from each group.

- Come to the event thinking about: How am I collecting my PCSC data, standardizing it for reporting to the USDA, and transforming or sharing it with multiple partners?  Where are the pain points? 
* We will generate a clear set of data collection journeys (events) and challenges built with domain experts and developers in the room.

**Enable knowledge exchange across projects**

* Compare data collection processes and question structures in order to collaboratively address and document shared challenges around USDA PCSC data reporting requirements

<br>

This is the day when we need as many PCSC project domain experts as possible! We will want each project group to map their data collection, use, and sharing needs across all steps and core partners within their project, with developers circulating to understand the needs and pain points of the process.

<br>

![group 2](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/group2.jpg?ref_type=heads)

<br>

* [Day 1 Notes](https://docs.google.com/document/d/10_yul7-XAJL8U7Hkx6N_tK22cha-jWGCNwEinSrb43Y/edit#heading=h.5l2nwc32s66b)

### Day 2

**Technical Synthesis**

* Create technical specifications for a common vocabulary solution
* Agree on 1 - 2 vetted technical solutions
* Assign developers to create a work plan to reshare with the group

<br>
This synthesis day will be more technical. PCSC domain experts are welcome to attend, but it is not expected.


<br>

![group 8](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/group8.jpg?ref_type=heads)

<br>


* [Day 2 Notes](https://docs.google.com/document/d/1Y0JLFujALJdG8nrqt_vhOh8YuQJ46JWz__w4cq-tQiI/edit#heading=h.6fm5fowlnog)
* [Photos](https://drive.google.com/drive/u/1/folders/1RgAubJtyn23t3qQz_uGU3iUDc2wKPliF)
* [Miro of all projects](https://miro.com/app/board/uXjVN_hTVBU=/)
* [PSCS Collaboration Proposal - Summary](https://docs.google.com/document/d/1JtP7eAMdmk0YGL73xAbFuEeS8X0aCCsm7ZARFQQO4jQ/edit#heading=h.rsyi0mpa8f42) - live document

## Followup Meeting & Proposal Review

*January 22, 2024 - 12:00-1:30pm EST*


[**Link to meeting Miro board**](https://miro.com/app/board/uXjVNPby6DM=/)


### Agenda 
* Reorient - why are we here
* Share updates & resources assembled since our in-person convening,
* Review and discuss two core conceptual proposals - are these on track?
* Identify the next steps / process for scoping proposals in more detail, and coming back as a group to generate support and resources
* Identify support and "champions" for detailed scoping of each proposal


### Discussion

- Presented documentation and group resources
- Provided background for participants who weren’t present at in-person event
- Introduced [draft proposal document](https://docs.google.com/document/d/1JtP7eAMdmk0YGL73xAbFuEeS8X0aCCsm7ZARFQQO4jQ/edit#heading=h.rsyi0mpa8f42)
* Consent to split discussion into 1) Workbook Automation (as starting point for schemas & ontologies) & 2) Claims Verification 


### Workbook Automation 

*These findings were summarized from participant discussions and notes.*

<br>

- USDA workbook is a common pain point, and functions as the lowest common denominator for data structure/processes. It is hyper-structured, and presents a more urgent need than pieces like COMET or CART.  The problem collectively is that although we have the same output, we all have different inputs. Can we as a cohort develop one step higher of a common denominator? 

<br>

>  “Would love to see the cluster get its core identity from the backend reporting. At its core, a USDA reporting (MVP), and then back into data schemas and structures” 

<br>

- Identifying which inputs and which outputs to prioritize is a critical part of the scope we need to determine. This indicates a necessary place for collaboration in this specific group– we can identify between us which inputs and outputs are important to multiple groups and why, without having to rely on individual assumptions. This is likely where the next conversation shield happen - detained design and scoping, using real projects, to compare where inputs and outputs overlap. 

<br>

> “ I feel like workbook automation (esp schemas/ontologies) will naturally be an evolving process. But maybe designing an iterative approach to tackle this would be useful.” 

<br>

### Claims Verification

*These findings were summarized from participant discussions and notes.* 

<br>

- Concept: software / tooling that can take evidence in (here's what happened), and according to some business rules, spits out what you qualify for based on what happened. 

- Way of assessing what incremental information is needed to access a qualification (incentive payments, certs, grants, labels, partnerships, etc.)
At the core, tooling stores rules for evidence / facts needed to qualify for a program.

The next pieces to determine are: 

1. Informationally: what kinds of data should be sucked in and spit out?
2. From a processing perspective, what is the needed internal logic? fulfilled criteria? creating a score?
3. Technical architecture - do we build from scratch? use existing software? where is it programmed?

<br>

![Evidence-To-Claims Engine](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/8abe5715a8bc7c45150e3376310ccde31fa9afa1/docs/img/evidence.claims.engine.png)
*Image from Troy Ruemping, IC-Foods*

<br>

>  “We see the claims engine as a solution for standardization and automation of verification (in the long term). In the short term, [Fibershed’s] PCSC project is struggling with understanding the USDA's requirements on monitoring and verification as it relates to CPS practices. Are we as a project able to define the MMRV criteria, or do we need to align with NRCS practice standards for that criteria?”

<br>

### Next steps: 

*  “Take a look at tooling that has been built, these things are connected but it would be helpful to demonstrate how - specifically for a claims system, it needs to know what a farm did, and that's what the conventions are trying to do”

*  “We can introduce existing tools (from all groups) but hope we can also really evaluate thinking from scratch and using the best of all approaches”

*  “How tightly integrated do these things need to be?”
* * “These tools are interdependent but developed independently”
 * * Loose coupling because it has to be - we need these to work as modules” 

<br>

Following this conversation, a time & space for existing tool demos was scheduled for February 2, 2024. 



### Outstanding questions - from Miro notes

>  “Now that we have our questions, how to bring more people in the room?” 

> “What additional work is needed to be ready to share?”

> “I'm curious about how we should think about the workbook automation work stream in terms of interim solution. How are programs going to think about completion of workbooks in the next 12 months until something is complete?”

<br>