# Process

## Background
This Partnership for Climate Smart Commodities (PCSC) Event Storming happened as part of an ongoing collaboration between teams at Our-Sci, LLC, OpenTEAM, and other partner organizations in our ecosystem. We identified a need based on a series of convergent workstreams that have happened in parallel over several years, and reached a critical point of convergence around the data needs of PCSC partners. Some of those workstreams include: 


- [Common Profile](https://www.our-sci.net/the-common-profile/) and [Question Set Library](https://www.our-sci.net/the-common-profile/) work by Our-Sci and OpenTEAM
- [farmOS conventions](https://farmos.discourse.group/t/documenting-conventions/1263) & [farm schema](https://farmos.discourse.group/t/farmos-convention-schemas-discussion/1615) work
- [Regen Network](https://www.regen.network/) data conventions, standards and protocols




## Timeline

*September 2023* - Reevaluation of Question Set Library structure in view of schemas for PCSC 

- Gitlab issue: [Question Set Library Near Term Needs](https://gitlab.com/OpenTEAMAg/feedback/support_requests/-/issues/170)

<br>


*October 2023* - Needs assessments with PCSC Action program partners. These meetings took the format of one-hour interviews between members of the OpenTEAM Tech Team and one or more representatives from each partner organization. 

 * Questions:

> * How did you initially design your data collection processes for the program you own?
> * How have those processes and needs changed in the context of PCSC/Action?
> * What data do you most value beyond the scope of the Action project? 

* Identified need for broader collaboration on PCSC process and interoperability. 

<br>

*October 2nd* - Began coordinating via weekly meetings between OpenTEAM and Our-Sci staff (Greg, Nat, Vic & Will) to plan event & define the scope of the project

* Gitlab Issue: [Community Notes PCSC Event Storming](https://gitlab.com/OpenTEAMAg/feedback/support_requests/-/issues/209)
* Challenge of defining project scope & attendees - discussion about generalizability and ‘archetype’ framing

<br>

*October 15th* - Outreach to potential partners

* [PCSC Event Storming One Pager](https://docs.google.com/document/d/1T2ABF1e3db8o8eiM5C0yYd-I2sXRBI5KWTCNYEo9xFU/edit#heading=h.a42kcdav62du)
* Location and dates chosen, invites sent.
* After 1-1 correspondence and calls with first round of invitees, identified the Bay Area as appropriate central location and either last week of November or second week of December as the target time. 

<br>

*October 23* - Second round of correspondence to ask each group who might be able to attend, and solicited feedback on which of the two date blocks would be more convenient

<br>

*October 24th* - Building Beta Data Management Protocols for Soil Carbon GHG Quantification Event Storming 

* [Post-Event Report](https://www.farmfoundation.org/2023/11/28/building-beta-data-management-protocols-for-soil-carbon-ghg-quantification/)
* Related partner project, also example & description of digital event storming

<br>

*November 6th* - 
Sent invite to developers for PCSC kickoff meeting, including agenda, background information, and logistics form for in-person gathering

* [Background for PCSC Event Storming - Devs](https://docs.google.com/document/d/1SUAR2R2_68_r1bgXsjDfNZsFgwfrvfAI_y-uETeBqXE/edit#heading=h.rc5et4fkdcce)

<br>

*November 8th* - Solidified attendee list and met to execute contracts for developers. 
Reviewed proposed venues & logistics

<br>

*November 14th* - Kickoff meeting with developers

* [Miro from event](https://miro.com/app/board/uXjVNPby6DM=/)
* Brief agenda: 
>  - *Defining the problem*: What even are Partnerships for Climate-Smart Commodities, context around USDA PCSC data workbook, data dictionary, & partner projects.
>   -  *Defining the field*: Applicability of the solution we're looking for beyond PCSC.
>   - *Defining the process*: How to engage & what's needed to prep for Event Storming.

<br>

*November 15th* - Followed up dev call with links to shared notes and resources, and invitation to shared Slack channel. 

- Booked shared housing for developers traveling from out of state
- Signed agreement with gallery space for the in-person event

<br>

*December 1st* - Met with Aaron Ault to discuss Event Storming process & application for our use case

  * Gitlab issue: [Community notes 12/1 Event Storming](https://gitlab.com/OpenTEAMAg/feedback/support_requests/-/issues/209#note_1715967249)

<br>

*December 5th* - Final session of FAIR Tech Ecosystem Registry Collabathon - Some overlap with attendees, registry process/concepts.

* [FAIR Tech Registry Collabathon Miro](https://miro.com/app/board/uXjVMq-cvco=/)

<br>

*December 11th* - Coordinating team arrives in the Bay - pre-event planning, supply purchase, key pickup and event space prep.



*December 12th* - Event Day 1. Mapping project-specific data processes and needs & finding where there are shared challenges across PCSC projects. 

<br>


* [Day 1 Notes](https://docs.google.com/document/d/10_yul7-XAJL8U7Hkx6N_tK22cha-jWGCNwEinSrb43Y/edit#heading=h.5l2nwc32s66b)

<br>

![Process](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/process.jpg?ref_type=heads)

<br>


*December 13th* - Event day 2 - technical synthesis and brainstorming of mutually-beneficial, interoperable solutions. 


* [Day 2 Notes](https://docs.google.com/document/d/1Y0JLFujALJdG8nrqt_vhOh8YuQJ46JWz__w4cq-tQiI/edit#heading=h.6fm5fowlnog)

<br>

![Process 4](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/process4.jpg?ref_type=heads)

<br>

### Summary and documentation



**Documents**

- [Shared notes folder](https://drive.google.com/drive/u/1/folders/1oW3AP76SzMIQauV8T0iWjvwIJTj7Heep)
- [Photos](https://drive.google.com/drive/u/1/folders/1RgAubJtyn23t3qQz_uGU3iUDc2wKPliF)
- [Miro of all projects](https://miro.com/app/board/uXjVN_hTVBU=/)
- [PSCS Collaboration Proposal - Summary](https://docs.google.com/document/d/1JtP7eAMdmk0YGL73xAbFuEeS8X0aCCsm7ZARFQQO4jQ/edit#heading=h.rsyi0mpa8f42) - live document


*Follow-up meeting scheduled for January 22nd, 2024* 