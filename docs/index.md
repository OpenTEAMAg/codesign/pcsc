# PCSC Event Storming - About

This Partnership for Climate Smart Commodities (PCSC) Event Storming happened as part of an ongoing collaboration between teams at Our-Sci, LLC, OpenTEAM, a diverse group of collaborators and developers, and representatives from seven PCSC projects in our ecosystem. 

- [About USDA Partnerships for Climate Smart Commodities](https://www.usda.gov/climate-solutions/climate-smart-commodities)
- [OpenTEAM](https://openteam.community/)
- [Our-Sci](https://www.our-sci.net/)
- [Participating Groups & Projects](https://openteamag.gitlab.io/codesign/pcsc/participants)

<br>



![Process 2](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/master/docs/img/process2.jpg?ref_type=heads)

<br>

## Problem Statement - October 2023
### The Problem
We all have similar challenges related to agricultural data collection, management, and sharing to meet the goals of our Partnership for Climate Smart Commodities projects. 

A huge amount of data is required for reporting at multiple points in time, and we need more or different data for our individual program needs. We have to transform the questions required for reporting into a format that makes sense for farmers and ranchers, then transform them again for collaboration with partners who can bring further value to the data collected.

We want to collect a maximally robust picture of farm activity to provide access to the greatest number of opportunities, while reducing the burden of data collection for farmers, technical assistance providers, and program managers. To do this, we must identify and standardize data we’ve collected for one purpose so it’s suitable for additional purposes. 


### The Goal
We aim to generate findable, accessible, interoperable and reusable (FAIR) agricultural vocabulary terms (keys, questions, or objects) which can be easily used for any data collection, use, or standard development project. In short – we want greater interoperability out of the gate, with bounded definitions that can be applied across different groups and projects. 


### The Process
We want to bring together a range of domain experts (people who manage data collection, build data standards, utilize data for analysis, etc) and technology experts (people who build data collection systems, data storage, and data design) for an Event Storming. Throughout the day we’ll describe our data collection process in terms of a series of events, noticing key decision points / failure points, and developing a shared understanding of where there is overlap and divergence across each project. 

### The Output
Through the Event Storming process, we intend to generate a mutually understood and vetted proposal for a FAIR agricultural vocabulary, with consensus and commitment to get it implemented.  

This will allow us all to benefit from comparable and reusable agricultural keys/questions/objects with bounded definitions that can be applied across different groups and projects, starting with some of the core shared needs across PCSC grant recipients.  


