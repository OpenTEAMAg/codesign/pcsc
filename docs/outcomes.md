# Outcomes

On December 12-13th, we had 28 participants from seven USDA Partnership for Climate Smart Commodities projects join us at Good Mother Studio in Oakland, CA to discuss their data management processes and challenges.

Through the Event Storming process, we intended to generate a mutually understood and vetted proposal for a FAIR agricultural vocabulary, with consensus and commitment to get it implemented. By conducting project-specific process mapping (event storming), followed by full-group identification of pain points, we met this goal and produced the following two tracks for subsequent work: 

**Schemas & Ontologies for Workbook Automation**

> This proposal is focused on building a farm data standard that enables transformation from different sources for export.  Auto-generating required data for the USDA reporting workbook will be our functional use case for knowledge mapping. 

**Evidence into Claims Engine**


> This proposal is organized around the concept of a service that ingests the totality of data types available to support a claim and generates verification or certification, based on a particular program’s requirements. 


While the first topic is tightly focused on the process from on-farm data collection to USDA reporting, and the second targets marketplace development and evidence-based indicators, we continue to see shared data vocabularies as a critical throughline for all aspects of this work.  

### Follow-Up & Proposal Review - January 22nd

[Meeting details are available here](https://openteamag.gitlab.io/codesign/pcsc/event/#followup-meeting-proposal-review). From this session, we determined that the two tracks we had previously identified would continue as proposed, and identified a need for tooling demonstrations and ongoing conversation concerning how integrated both of these workstreams should be. 

## Common Farm Schema - Ongoing Work

### Tech Tooling Demos - February 2nd

[Zoom Recording of session](https://us02web.zoom.us/rec/share/aurtzjVo9ZqqY2hWqn6TH6YXlhEPjq1YhWq78l1F98tTHz9KhbR52GedCPjvrXA.g-CisuwNopBSot29?startTime=1706893366000)

*Passcode: =b6abYJY*

This meeting was held to demonstrate existing tooling in order to ground the next round of scoping conversations. Agenda focused on tools & processes created to work with the Common Farm Convention, followed by Q&A & application discussions.

#### Meeting Resources

- [Our-Sci - Farm Conventions Schema documentation](https://our-sci.gitlab.io/software/json_schema_distribution/wiki/)
- [farmOS Conventions Schema Builder library](https://conventions-builder-our-sci-software-6938826f05a4d67f44a5939331.gitlab.io/)
- [farmOS Conventions Forum](https://farmos.discourse.group/c/conventions/11)
- [Conventions Community Call](https://meet.jit.si/farmos-dev) - Occurs every two weeks on Thursdays from 1:00pm-1:45pm EST. 

    > **About**
    > 
    > Call for discussing the ag data wallet / common farm format / FarmOS Conventions / schema distribution gitlab / etc.  We'll call this all Conventions for simplicity.
    > 
    > For developers actively working on Conventions, or those interested in learning more or creating a translation from another format (API or other).



### Tooling Descriptions

![ADW Vision](https://gitlab.com/OpenTEAMAg/codesign/pcsc/-/raw/4bc76d21b3c3778c3dcd6ec5a04bdff480fcf46a/docs/img/adwvision.png)

**Common Farm Convention (beta)**

Contacts: Emily Brady, Octavio Duarte, Greg Austic

> Developed through OpenTEAM with ongoing collaborations from USDA
>
> Modern design using JSON Schema.  Includes tooling for automatic human and machine readable publishing (example documentation) and validation.
>
> Existing funding through OpenTEAM and USDA to create additional Conventions which map to Cool Farm Tool and COMET Farm, bi-weekly call and forum for support.


**Farm Convention Publisher (beta)**

Contacts: Octavio Duarte, Greg Austic, Emily Brady

> Simplifies high-confidence modification and publishing of custom Conventions using the Common Farm Convention format.
> 
> Includes tooling for automatic human and machine readable publishing of schemas (example documentation), code for schema validation, and maintaining compatibility between Conventions (parent -> child relationships)

**Farm Convention Libraries (beta)**

Contacts: Octavio Duarte

> JS NPM library which enables simpler and less error prone management of schemas
> 
> Add, modify, edit, and assemble schemas using more intuitive helper functions which utilize raw JSON Schema rules in a more consistent manner.

**Common Farm Convention Base Schema (beta)**
Contacts: Emily Brady, Greg Austic

> A published set of typical Events (Tillage, Planting, Fertilizer, Pesticides, Grazing, etc.) which can contain most event-based agricultural data (documentation)

**Custom Airtable for Data Dictionaries + Mapping (alpha)**
Contacts: Juliet Norton, Emily Brady

> Automatically outputs data in a schema to rows in a Airtable base to create a data dictionary
Standardizes mapping from one data dictionary to another in a human readable, accessible way.
> 
> Allows non-technical domain experts to create highly accurate translations with minimal assumptions / errors.
> 
> Produces highly accurate and repeatable requirements documents for technical staff to write a translator in JS.

**API Switchboard (alpha)**

Contacts: Kevin Brown (CSU, Axios), Octavio Duarte, Greg Austic

> Web or local service that accepts data in the Common Farm Convention format
> 
> Uses a translation file to convert to the output format (COMET, CFT, Workbook, etc.)
>  
> Returns output format with any errors, warnings, etc.
> 
> Can be run as a web service or locally.

### Common Farm Conventions Community Call

Happening biweekly on alternate Thursdays, 1:00pm-1:45pm EST

- [Conventions discussion forum](https://farmos.discourse.group/c/conventions/11)
- [Meeting Link](https://meet.jit.si/farmos-dev)
- [Google Calendar Invite Link](https://calendar.google.com/calendar/event?action=TEMPLATE&tmeid=ZjI0NGFlYzM0cjNhZHVuZm1mNGZvZGxuamxfMjAyNDAyMjlUMTgwMDAwWiB2aWNAb3BlbnRlYW0uY29tbXVuaXR5&tmsrc=vic%40openteam.community&scp=ALL) 

For developers actively working on Conventions, or those interested in learning more or creating a translation from another format (API or other).

Potential topics to share:

- What updates (tools, translations, libraries, implementations) do you have re. how you’re using Conventions?
- How are you considering using Conventions?
- What problems are you having? How could it be better?
- What questions do you have about using JSON Schema, and best practices?
- How should we be thinking about Conventions as a community?